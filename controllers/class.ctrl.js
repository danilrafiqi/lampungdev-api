const Class = require('../db/models/class');
module.exports = {
  all: (req, res) => {
    Class.find({}, (err, data) => {
      if (err) throw err;
      res.send(data);
    });
  },
  detail: (req, res) => {
    Class.findOne({ slug: req.params.slug }, (err, data) => {
      if (err) throw err;
      res.send(data);
    });
  },
  post: (req, res) => {
    Class.create(req.body, (err, data) => {
      if (err) throw err;
      res.send('success create data');
    });
  },
  edit: (req, res) => {
    Class.findOneAndUpdate({ slug: req.params.slug }, req.body, (err, data) => {
      if (err) throw err;
      res.send('succes edit');
    });
  },
  delete: (req, res) => {
    Class.findOneAndRemove({ slug: req.params.slug }, err => {
      if (err) throw err;
      res.send('success delete');
    });
  }
};
