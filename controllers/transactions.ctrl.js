const Transactions = require('../db/models/transactions');
module.exports = {
  all: (req, res) => {
    Transactions.find({})
      .populate('classId')
      .exec((err, data) => {
        if (err) throw err;
        res.send(data);
      });
  },
  detail: (req, res) => {
    Transactions.findById(req.params.id)
      .populate('classId')
      .exec((err, data) => {
        if (err) throw err;
        res.send(data);
      });
  },
  post: (req, res) => {
    Transactions.create(req.body, (err, data) => {
      if (err) throw err;
      res.send('success create data');
    });
  },
  edit: (req, res) => {
    Transactions.findByIdAndUpdate(req.params.id, req.body, (err, data) => {
      if (err) throw err;
      res.send('succes edit');
    });
  },
  delete: (req, res) => {
    Transactions.findByIdAndRemove(req.params.id, err => {
      if (err) throw err;
      res.send('success delete');
    });
  }
};
