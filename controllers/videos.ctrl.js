const Videos = require('../db/models/videos');
module.exports = {
  all: (req, res) => {
    Videos.find({})
      .populate('classId')
      .exec((err, data) => {
        if (err) throw err;
        res.send(data);
      });
  },
  detail: (req, res) => {
    Videos.findOne({ slug: req.params.slug })
      .populate('classId')
      .exec((err, data) => {
        if (err) throw err;
        res.send(data);
      });
  },
  //query untuk nampilin daftar video perkelas
  kelas: (req, res) => {
    Videos.find({ 'classId.name': req.params.name }).exec((err, data) => {
      if (err) throw err;
      res.send(data);
    });
  },
  post: (req, res) => {
    Videos.create(req.body, (err, data) => {
      if (err) throw err;
      res.send('success create data');
    });
  },
  edit: (req, res) => {
    Videos.findOneAndUpdate(
      { slug: req.params.slug },
      req.body,
      (err, data) => {
        if (err) throw err;
        res.send('succes edit');
      }
    );
  },
  delete: (req, res) => {
    Videos.findOneAndRemove({ slug: req.params.slug }, err => {
      if (err) throw err;
      res.send('success delete');
    });
  }
};
