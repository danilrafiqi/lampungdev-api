const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tansactionsSchema = new Schema(
  {
    userEmail: { type: String, required: true },
    classId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Class'
    }
  },
  {
    timestamps: true
  }
);

const Tansaction = mongoose.model('Tansaction', tansactionsSchema);

module.exports = Tansaction;
