const express = require('express');
const router = express.Router();
const classCtrl = require('../controllers/class.ctrl');

router.get('/', classCtrl.all);
router.get('/:slug', classCtrl.detail);
router.post('/', classCtrl.post);
router.put('/:slug', classCtrl.edit);
router.delete('/:slug', classCtrl.delete);

module.exports = router;
