const express = require('express');
const router = express.Router();

// import routes
const userRoute = require('./users.routes');
const classRoute = require('./class.routes');
const videosRoute = require('./videos.routes');
const transactionsRoute = require('./transactions.routes');

// use routes
router.use('/user', userRoute);
router.use('/class', classRoute);
router.use('/video', videosRoute);
router.use('/transaction', transactionsRoute);

module.exports = router;
