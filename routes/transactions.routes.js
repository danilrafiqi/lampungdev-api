const express = require('express');
const router = express.Router();
const transactionCtrl = require('../controllers/transactions.ctrl');

router.get('/', transactionCtrl.all);
router.get('/:id', transactionCtrl.detail);
router.post('/', transactionCtrl.post);
router.put('/:id', transactionCtrl.edit);
router.delete('/:id', transactionCtrl.delete);

module.exports = router;
