const express = require('express');
const router = express.Router();
const users = require('../controllers/users.ctrl')

router.get('/', users.all);
router.get('/:id', users.detail);
router.post('/', users.post);
router.put('/:id', users.edit);
router.delete('/:id', users.delete);

module.exports = router;