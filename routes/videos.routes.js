const express = require('express');
const router = express.Router();
const videosCtrl = require('../controllers/videos.ctrl');

router.get('/', videosCtrl.all);
router.get('/:slug', videosCtrl.detail);
router.get('/:kelas/kelas', videosCtrl.kelas);
router.post('/', videosCtrl.post);
router.put('/:slug', videosCtrl.edit);
router.delete('/:slug', videosCtrl.delete);

module.exports = router;
