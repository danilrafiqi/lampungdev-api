const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const morgan = require('morgan');
mongoose.connect(
  'mongodb://danil:rahasia12@ds129374.mlab.com:29374/lampungdev',
  { useNewUrlParser: true }
);

const app = express();
app.use(morgan('dev'));

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const route = require('./routes/index');
app.use(route);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

/* LISTENING */
const port = process.env.PORT || 8000;
app.listen(port, process.env.IP, () => {
  console.log('Server Started on Port: ', port);
});

module.exports = app;
